const fs = require("fs");

function readingFile(filename) {
    return new Promise((resolve, reject) => {
       // setTimeout(()=> {
            fs.readFile(filename, "utf-8", (err,data) => {
                if(err) {
                    reject(err);
                }
                else {
                    resolve(data);
                }
            });
    
       // },15*1000);
    });
}

module.exports = readingFile;