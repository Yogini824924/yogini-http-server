const http = require("http");
const {v4} = require("uuid");
const readingFile = require("./readingFile.js");


const PORT = 5000;

const server = http.createServer((request, response) => {
    console.log(request.url);

    switch(request.url) {
        case "/html": {
            readingFile("./index.html")
            .then((data) => {
                response.writeHead(200, {
                    "Content-Type" : "text/html"
                });
                response.write(data);
                response.end();
            })
            .catch((err) => { 
                response.writeHead(500, {
                    "Content-Type" : "text/html"
                });
                response.write("html file not found");
                response.end();
            });
            break;
        }

        case "/json": {
            readingFile("./index.json")
            .then((data) => {
                response.writeHead(200, {
                    "Content-Type" : "application/json"
                });
                response.write(JSON.stringify(JSON.parse(data), null, 4));
                response.end();
            })
            .catch((err) => {
                response.writeHead(500, {
                    "Content-Type" : "application/json"
                });
                response.write("json file not found");
                response.end();
            });
            break;
        }

        case "/uuid": {
            response.writeHead(200, {
                "Content-Type" : "application/json"
            });
            response.write(JSON.stringify(
                { 
                    "uuid" : v4()
                }, 
                null, 4
            ));
            response.end();
            break;
        }

        case "/status/"+request.url.split("/")[2]: {
            let code = request.url.split("/")[2];
            if(code in http.STATUS_CODES) {
                response.writeHead(code, {
                    "Content-Type" : "text/html"
                 });
                response.write(`<strong>${code}</strong>`);
                response.end();
                break;
            }
            else {
                response.writeHead(404, {
                    "Content-Type" : "text/plain"
                });
                response.write(`${request.url} try for another status code`);
                response.end();
                break;
            }
        }

        case "/delay/"+request.url.split("/")[2]: {
            let delay = request.url.split("/")[2];
            if(!isNaN(delay) && delay >= 0) {
                console.log(`Waiting to complete ${delay} seconds`);
                setTimeout(() => {
                    response.writeHead(200);
                    response.write(`Success response after ${delay} seconds with status code ${response.statusCode}`);
                    response.end();
                }, delay*1000);
                break;
            }
            else {
                response.writeHead(404);
                response.write("Enter a valid delay");
                response.end();
                break;
            }
        }

        default: {
            response.writeHead(404, {
                "Content-Type" : "text/plain"
            });
            response.write(`${request.url} page not found`);
            response.end();
            break;
        }
    }
});

server.listen(PORT);